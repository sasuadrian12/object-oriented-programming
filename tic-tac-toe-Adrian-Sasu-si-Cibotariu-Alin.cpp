#include <iostream>
#include <list>
#include <ctime>

using namespace std;
using std::string;

typedef struct
{
    int *row;
} WinList;

class Player
{
private:
    string name;
    int score;

public:
    Player() : Player("") {}
    Player(string n) : score(0), name(n) {}

    void won()
    {
        score++;
    };

    int getScore()
    {
        return score;
    };

    string getName()
    {
        return name;
    }
};

class Game
{
private:
    char board[9];
    int emptyIndex[9];
    int againstComputer, gameOn;
    int emptyCount;
    WinList winList[8];

    void playerInput(Player &player)
    {
        int pos;
        cout << endl;
        cout << player.getName() << "'s turn" << endl;
        cout << "Enter position (1-9): ";

        // Read user input as a string
        string input;
        cin >> input;

        // Check if the input consists of a single digit
        if (input.length() == 1 && isdigit(input[0]))
        {
            // Convert the input string to an integer
            pos = input[0] - '0';

            if (pos >= 1 && pos <= 9) // Check if the position is valid
            {
                pos -= 1;

                if (emptyIndex[pos] == 1)
                {
                    cout << "Position already taken" << endl;
                    playerInput(player);
                }
                else
                {
                    emptyIndex[pos] = 1;
                    emptyCount -= 1;
                    if (player.getName() == "Player 1")
                        board[pos] = 'O';
                    else
                        board[pos] = 'X';
                }
            }
            else
            {
                cout << "Invalid position. Please enter a number between 1 and 9." << endl;
                playerInput(player);
            }
        }
        else
        {
            cout << "Invalid input. Please enter a number from 1 to 9." << endl;
            playerInput(player);
        }
    }

    void computerInput()
    {
        int pos;
        pos = rand() % 9;
        if (emptyIndex[pos] == 1)
        {
            if (emptyCount < 0)
            {
                return;
                // recursive
                computerInput();
            }
        }
        else
        {
            cout << "Computer player at " << pos + 1 << endl;
            emptyIndex[pos] = 1;
            emptyCount -= 1;
            board[pos] = 'O';
        }
    }

    void displayBoard()
    {
        cout << endl;
        cout << "\t   |   |   " << endl;
        cout << "\t " << board[0] << " | " << board[1] << " | " << board[2] << endl;
        cout << "\t   |   |   " << endl;

        cout << "-------------" << endl;

        cout << "\t   |   |   " << endl;
        cout << "\t " << board[3] << " | " << board[4] << " | " << board[5] << endl;
        cout << "\t   |   |   " << endl;

        cout << "-------------" << endl;

        cout << "\t   |   |   " << endl;
        cout << "\t " << board[6] << " | " << board[7] << " | " << board[8] << endl;
        cout << "\t   |   |   " << endl;
    }

    void checkWin(Player &p1, Player &p2)
    {
        for (int i = 0; i < 8; i++)
        {
            int *row = winList[i].row;
            if (board[row[0]] == 'X' && board[row[1]] == 'X' && board[row[2]] == 'X')
            {
                cout << p1.getName() << " wins!" << endl;
                p1.won();
                gameOn = 0;
                return;
            }
            else if (board[row[0]] == 'O' && board[row[1]] == 'O' && board[row[2]] == 'O')
            {
                if (againstComputer)
                    cout << "Computer wins!" << endl;
                else
                    cout << p2.getName() << " wins!" << endl;
                p2.won();
                gameOn = 0;
                return;
            }
        }
        if (emptyCount <= 0)
        {
            cout << "It's a tie!" << endl;
            gameOn = 0;
        }
    }

    void play(Player &p1, Player &p2)
    {
        char rematch = '\0';
        int hand = 0;
        gameOn = 1;
        displayBoard();

        while ((emptyCount > 0) && (gameOn != 0))
        {
            if (againstComputer)
            {
                hand == 0 ? computerInput() : playerInput(p2);
            }
            else
            {
                hand == 0 ? playerInput(p1) : playerInput(p2);
            }

            hand = !hand;
            displayBoard();
            checkWin(p1, p2);
        }

        if (emptyCount <= 0)
        {
            cout << "\t No WINNER" << endl;
        }

        cout << "----------------------------" << endl;
        cout << "Rematch? (y/n)" << endl;

        cin >> rematch;

        if (rematch == 'y' || rematch == 'Y')
        {
            resetBoard();
            play(p1, p2);
        }
    }

    void displayScore(Player &p1, Player &p2)
    {
        cout << endl;
        cout << "\t SCORE" << endl;

        if (againstComputer)
        {
            cout << "Player 1: " << p1.getName() << "\t Computer: " << p2.getScore() << endl;
        }
        else
        {
            cout << "Player 1: " << p1.getName() << "\t Player 2: " << p2.getScore() << endl;
        }
    }

public:
    Game() : emptyCount(0), gameOn(1), againstComputer(0)
    {
        resetBoard();
        winList[0].row = new int[3]{0, 1, 2};
        winList[1].row = new int[3]{3, 4, 5};
        winList[2].row = new int[3]{6, 7, 8};
        winList[3].row = new int[3]{0, 3, 6};
        winList[4].row = new int[3]{1, 4, 7};
        winList[5].row = new int[3]{2, 5, 8};
        winList[6].row = new int[3]{0, 4, 8};
        winList[7].row = new int[3]{2, 4, 6};
    }

    void resetBoard()
    {
        gameOn = 1;
        emptyCount = 0;

        srand(time(0));

        for (size_t i = 0; i < 9; i++)
        {
            emptyIndex[i] = 0;
            board[i] = (i + 1) + '0';
            emptyCount++;
        }
        emptyCount--;
    }

    void onePlayerGame()
    {
        Player p("Player 1");
        Player c("Computer");

        cout << "-----------" << endl;
        cout << "\t Player 1: X \t Computer: O" << endl;
        cout << "-----------" << endl;
        cout << endl;
        againstComputer = 1;
        play(p, c);
    }

    void twoPlayerGame()
    {
        Player p("Player 1");
        Player c("Player 2");

        cout << "----------------------------" << endl;
        cout << "\t Player 1: X \t Player 2: O" << endl;
        cout << "----------------------------" << endl;
        againstComputer = 0;
        play(p, c);
    }
};

int main()
{
    cout << "Hello" << endl;
    int ch;
    while (1)
    {
        cout << "--------------Menu--------------" << endl;
        cout << "\t 1. 1 Player Game" << endl;
        cout << "\t 2. 2 Player Game" << endl;
        cout << "\t 3. Exit" << endl;
        cout << "----------------------------" << endl;
        cout << "Enter your choice: ";

        cin >> ch;

        switch (ch)
        {
        case 1:
        {
            Game *game = new Game();
            game->resetBoard();
            game->onePlayerGame();
            break;
        }
        case 2:
        {
            Game *game = new Game();
            game->resetBoard();
            game->twoPlayerGame();
            break;
        }
        case 3:
            return 0;
        default:
            cout << "Invalid Choice" << endl;
        }
    }

    return 0;
}
